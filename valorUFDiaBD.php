<?php
	// header('Access-Control-Allow-Origin: *');
	date_default_timezone_set('America/Argentina/Buenos_Aires');
	require('consultas.php');
	// ini_set('display_errors', 'On');

	$ch = curl_init();

	$fecha = new Datetime();
	$fecha = $fecha->format('d-m-Y');

	$fechaFin = date("d/m/Y",strtotime($fecha."+ 32 days"));
	$fecha = date("d/m/Y",strtotime($fecha."+ 0 days"));

	while(compararFechas($fecha,$fechaFin) < 0){
		$fecha = date("d-m-Y",strtotime(str_replace("/","-",$fecha)."+ 0 days"));
		$trozo = explode("-",$fecha);
		$ano = $trozo[2];
		$mes = $trozo[1];
		$dia = $trozo[0];
		// echo explode("-",$fecha)[2] . "-" . explode("-",$fecha)[1] . "-" . explode("-",$fecha)[0] . "<br>";
		// set url
		curl_setopt($ch, CURLOPT_URL, "https://mindicador.cl/api/uf/" . $fecha);

 	 //return the transfer as a string
 	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

 	 // $output contains the output string
 	 $output = json_decode(curl_exec($ch));

 	 if(is_null($output) || (string)$output->serie[0]->valor == ''){
 		 curl_setopt($ch, CURLOPT_URL, "https://api.sbif.cl/api-sbifv3/recursos_api/uf/" . $ano . "/" . $mes . "/dias/" . $dia . "?apikey=eea14d71d1dbdcfd6230a256a86794911ff53f66&formato=json");

 		 //return the transfer as a string
 		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

 		 // $output contains the output string
 		 $output = json_decode(curl_exec($ch));

 		 $valor = $output->UFs[0]->Valor;
 		 $valor = str_replace('.','',$valor);
 		 $valor = str_replace(',','.',$valor);
 	 }
 	 else{
 		 $valor = (string)$output->serie[0]->valor;
 	 }

		// echo number_format($valor, 2, '.', '') . "<br>";

		if($valor != ''){
			ingresaValorUF(number_format($valor, 2, '.', ''),explode("-",$fecha)[2] . "-" . explode("-",$fecha)[1] . "-" . explode("-",$fecha)[0]);
			echo explode("-",$fecha)[2] . "-" . explode("-",$fecha)[1] . "-" . explode("-",$fecha)[0] . " / " . number_format($valor, 2, '.', '') . "\n";
		}

		//Sumamos un día
		$fecha = date("d/m/Y",strtotime($fecha."+ 1 days"));
	}

	function compararFechas($primera, $segunda){
		  $valoresPrimera = explode ("/", $primera);
		  $valoresSegunda = explode ("/", $segunda);

		  $diaPrimera    = $valoresPrimera[0];
		  $mesPrimera  = $valoresPrimera[1];
		  $anyoPrimera   = $valoresPrimera[2];

		  $diaSegunda   = $valoresSegunda[0];
		  $mesSegunda = $valoresSegunda[1];
		  $anyoSegunda  = $valoresSegunda[2];

		  $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
		  $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);

		  if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
		    // "La fecha ".$primera." no es v&aacute;lida";
		    return 0;
		  }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
		    // "La fecha ".$segunda." no es v&aacute;lida";
		    return 0;
		  }else{
		    return  $diasPrimeraJuliano - $diasSegundaJuliano;
		  }
		}
	?>
