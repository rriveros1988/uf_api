<?php
	header('Access-Control-Allow-Origin: *');
	date_default_timezone_set('America/Argentina/Buenos_Aires');
	require('consultas.php');

	if(count($_GET) > 0){
		// $ch = curl_init();

		$fecha = new Datetime($_GET['fecha']);
		$fecha = $fecha->format('Y-m-d');

		$valor = consultaValorUfDia($fecha);

		if(is_null($valor[0]['VALOR'])){
			echo '';
		}
		else
		{
			echo number_format($valor[0]['VALOR'], 2, ',', '.');
		}
	}
	else{
  	echo "Sin datos";
  }
?>
